<?php
/**
 * @file form_confront.admin.inc
 */

/**
 * Implements hook_form()
 */
function form_confront_admin_form($form, &$form_state) {

    /**
     * Webform to View Mapping
     */
    $form['webform_view_map'] = array (
        '#type'         => 'fieldset',
        '#description'  => t('Map webforms to views. Only one webform can be set to confront a given view.'),
        '#title'        => t('Webform To View Map'),
        '#collapsible'  => true,
    );

    $form['webform_view_map']['select_view_view'] = array (
        '#title'        => t('View'),
        '#description'  => t('Select a view to be confronted'),
        '#type'         => 'select',
        '#options'      => _form_confront_get_view_list(),
        '#empty_option' => '-- select --',
    );

    $form['webform_view_map']['select_view_webform'] = array (
        '#title'        => t('Webform'),
        '#description'  => t('Select a webform to confront a view with'),
        '#type'         => 'select',
        '#options'      => _form_confront_get_webform_list(),
        '#empty_option' => '-- select --',
    );

    $form['webform_view_map']['submit_view_map'] = array (
        '#type'     => 'submit',
        '#value'    => t('Submit'),
        '#submit'   => array('_form_confront_submit_mapping'),
    );

    $form['webform_view_map']['webform_view_mapping'] = array (
        '#markup' => _form_confront_build_mapping_table('view'),
    );

    /**
     * Webform to Content Type Mapping
     */
    $form['webform_content_type_map'] = array (
        '#type'         => 'fieldset',
        '#description'  => t('Map webforms to content types. Only one webform can be set to confront a given content type.
            This means a webform has to be filled before viewing an item of this content type'),
        '#title'        => t('Webform To Content Type Map'),
        '#collapsible'  => true,
    );

    $form['webform_content_type_map']['select_content_type'] = array (
        '#type'          => 'select',
        '#title'        => t('Content Type'),
        '#description'  => t('Select a content type to be confronted'),
        '#options'      => _form_confront_get_content_type_list(),
        '#empty_option' => '-- select --',
    );

    $form['webform_content_type_map']['select_content_type_webform'] = array (
        '#title'        => t('Webform'),
        '#description'  => t('Select a webform to confront a content_type with'),
        '#type'         => 'select',
        '#options'      => _form_confront_get_webform_list(),
        '#empty_option' => '-- select --',
    );

    $form['webform_content_type_map']['submit_content_type_map'] = array (
        '#type'     => 'submit',
        '#value'    => t('Submit'),
        '#submit'   => array('_form_confront_submit_mapping'),
    );

    $form['webform_content_type_map']['webform_content_type_mapping'] = array (
        '#markup' => _form_confront_build_mapping_table('content type'),
    );

    return system_settings_form($form);
}

/**
 * form_confront_delete_mapping()
 * @description
 *  delete a mapping by id (i.e. content type/vid), redirects back to the config
 * @param $id
 */
function form_confront_delete_mapping($container, $id) {
    $id = $id ? $id : "0";

    // all mapping is done via json file
    $path = dirname(__FILE__) . '/form_confront.map.json';
    $json = array();
    if (file_exists($path)) {
        $contents = file_get_contents($path);
        $json = json_decode($contents, true);
    }

    if (array_key_exists($container, $json)) {
        unset($json[$container][$id]);
        $file = fopen($path, 'w');
        fwrite($file, json_encode($json));
        fclose($file);
        drupal_set_message(t("$container %id removed from mapping", array("%id"=>$id)));
    }

    drupal_goto('admin/content/form_confront_config');
}

/**
 * _form_confront_submit_mapping()
 * @description
 *  submit handler for mapping a view/path to a webform
 * @param $form
 * @param $form_state
 */
function _form_confront_submit_mapping($form, &$form_state) {
    $vid = (string) $form_state['values']['select_view_view'];
    $view_wid = (string) $form_state['values']['select_view_webform'];

    $content_type = (string) $form_state['values']['select_content_type'];
    $content_type_wid = (string) $form_state['values']['select_content_type_webform'];

    if (!empty($vid) && !empty($view_wid)) {
        _form_confront_add_map_values($vid,$view_wid);
    }
    if (!empty($content_type) && !empty($content_type_wid)) {
        _form_confront_add_map_values($content_type,$content_type_wid, 'content type');
    }
}

/**
 * _form_confront_add_map_values()
 * @description
 *  map a view to a webform
 * @param $id
 * @param $wid
 * @param $container
 *  type of mapping i.e. view, path etc
 */
function _form_confront_add_map_values($id, $wid, $container='view') {
    $id = $id ? $id : "0";
    $wid = $wid ? $wid : "0";

    // all mapping is done via json file
    $path = dirname(__FILE__) . '/form_confront.map.json';
    $json = array();
    if (file_exists($path)) {
        $contents = file_get_contents($path);
        $json = drupal_json_decode($contents);
    }

    $json[$container][$id] = $wid;
    $json[$container] = array_unique($json[$container]);

    $file = fopen($path, 'w');
    fwrite($file, drupal_json_encode($json));
    fclose($file);
}

/**
 * _form_confront_get_view_list()
 * @description
 *  get a select list of views by vid/human_name
 * @return array
 */
function _form_confront_get_view_list() {
    $select = array();

    foreach(views_get_all_views() as $view) {
        $select[(int) $view->vid] = $view->human_name;
    }

    return $select;
}

/**
 * _form_confront_build_view_mapping_table()
 * @description
 *  use the json to get a table of mapped view values
 *  for reference.
 * @return string
 * @throws Exception
 */
function _form_confront_build_mapping_table($container='view') {

    $header = array($container, "Webform ID", "Delete");
    $views = _form_confront_get_view_list();
    $webforms = _form_confront_get_webform_list();
    $rows = array();

    $path = dirname(__FILE__) . '/form_confront.map.json';
    $json = array();
    if (file_exists($path)) {
        $contents = file_get_contents($path);
        $json = json_decode($contents, true);
    }

    if (array_key_exists($container, $json)) {

        // map the view name and webform name for each row, otherwise a "NOT FOUND" error
        foreach ($json[$container] as $vid=>$wid) {
            $link = l(t("Delete"), "admin/content/form_confront_config/delete/$container/$vid");

            if ($container == 'view') {
                if (array_key_exists($vid, $views)) {
                    $vid = $views[$vid];
                } else {
                    $vid = "VIEW NOT FOUND";
                }
            }

            if (array_key_exists($wid, $webforms)) {
                $wid = $webforms[$wid];
            }
            else {
                $wid = "WEBFORM NOT FOUND";
            }

            $rows[] = array($vid, $wid, $link);
        }

        if (count($rows)) {
            return theme('table', array('header' => $header, 'rows' => $rows));
        }

    }

    return t("No mappings added yet, add one to view the mapping table");
}

/**
 * _form_confront_get_webform_list()
 * @description
 *  return a select box of available webforms
 *  mapping webform nid to title
 * @return array
 */
function _form_confront_get_webform_list() {
    $select = array();
    $nids = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->fields('n', array('type'))
        ->condition('n.type', 'webform')
        ->execute()
        ->fetchCol(); // returns an indexed array

    $nodes = node_load_multiple($nids);
    foreach ($nodes as $node) {
        $select[$node->nid] = $node->title;
    }

    return $select;
}

/**
 * _form_confront_get_content_type_list()
 * @description
 *  return a select box of available content type
 *  mappings id to title
 * @return array
 */
function _form_confront_get_content_type_list() {
    $select = array();
    $nids = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->fields('n', array('type'))
        ->condition('n.type', 'webform')
        ->execute()
        ->fetchCol(); // returns an indexed array

    $types = node_type_get_types();
    foreach ($types as $type) {
        $select[$type->type] = $type->name;
    }

    return $select;
}
